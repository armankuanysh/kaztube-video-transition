let pageAnimation = () => {

    var controller = new ScrollMagic.Controller();

    const header = $('.header')
    const topimage = $('#top-image')
    const topimage2 = $('#top-image2')
    const toptitle = $('#services .section-title')
    const toptext = $('#services .section-text')

    let headerIntro = new TimelineMax();
    let topimageIntro = new TimelineMax();
    var tween = new TimelineMax()
    var tweenAdmin = new TimelineMax()
    var button = new TimelineMax()
    var logos = new TimelineMax()
    let toptextIntro = new TimelineMax();
    let topTitleIntro = new TimelineMax();
    let feedbackWindow = new TimelineMax();

    let slideTl = new TimelineMax();

    slideTl
        // появление первой картинки
        .from(topimage, 1.5, {
            opacity: 0,
            yPercent: -20,
            ease: Cubic.easeOut
        })
        // появление первой картинки
        .from(topimage2, 1.5, {
            opacity: 0,
            yPercent: -20,
            ease: Cubic.easeOut
        }, '-=1.5')
        // появление первого заголовка
        .from(toptitle, 1, {
            // delay: 1,
            opacity: 0,
            yPercent: -30,
            ease: Cubic.easeOut
        }, '-=.8')

        // появление первого текста
        .from(toptext, 1, {
            // delay: 1.5,
            opacity: 0,
            yPercent: -20,
            ease: Cubic.easeOut
        }, '-=.4')

        // появление хэдера
        .from(header, 1, {
            // delay: 2,
            opacity: 0,
            yPercent: -20,
            ease: Cubic.easeOut
        }, '-=.4');

    tweenAdmin.fromTo('#robokassa img', 1, {
        yPercent: (window.innerWidth < 991) ? 60 : 20
    }, {
        yPercent: 0,
        ease: Cubic.easeOut
    })

    // скрины сайтов
    tween.add([
        TweenMax.fromTo("#parallax-image1", 1, {
            yPercent: (window.innerWidth < 991) ? 60 : 30,
        }, {
            yPercent: (window.innerWidth < 991) ? 0 : -30,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#parallax-image3", 1, {
            yPercent: (window.innerWidth < 991) ? 110 : 80,
        }, {
            yPercent: (window.innerWidth < 991) ? 0 : -30,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#parallax-image2", 1, {
            yPercent: (window.innerWidth < 991) ? 70 : 40,
        }, {
            yPercent: (window.innerWidth < 991) ? 0 : -30,
            ease: Cubic.easeOut
        })

    ]);

    button.fromTo("#link-more", 1, {
        yPercent:  (window.innerWidth < 991) ? 70 : 50,
    }, {
        yPercent:  (window.innerWidth < 991) ? 0 : -100,
        ease: Cubic.easeOut
    })

    // сцена с параллаксом скринов
    let parallaxScene = new ScrollMagic.Scene({
            triggerElement: "#services2",
            duration: '100%'
        })
        .setTween(tween)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    // сцена с параллаксом кнопки
    let parallaxButton = new ScrollMagic.Scene({
            triggerElement: "#services2",
            duration: '100%',
            triggerHook: 'onLeave'
        })
        .setTween(button)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);


    let adminScene = new ScrollMagic.Scene({
            triggerElement: "#robokassa",
            duration: '100%'
        })
        .setTween(tweenAdmin)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    // лого патнеров
    logos.add([
        TweenMax.fromTo("#partner-logo1", 1, {
            yPercent: 160
        }, {
            yPercent: -30,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#partner-logo2", 1, {
            yPercent: 220
        }, {
            yPercent: 20,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#partner-logo3", 1, {
            yPercent: 170
        }, {
            yPercent: -65,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#partner-logo4", 1, {
            yPercent: 170
        }, {
            yPercent: -20,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#partner-logo5", 1, {
            yPercent: 160
        }, {
            yPercent: -20,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#partner-logo6", 1, {
            yPercent: 60
        }, {
            yPercent: -30,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#partner-logo7", 1, {
            yPercent: 70
        }, {
            yPercent: -20,
            ease: Cubic.easeOut
        }),
        TweenMax.fromTo("#partner-logo8", 1, {
            yPercent: 60
        }, {
            yPercent: -20,
            ease: Cubic.easeOut
        })
    ]);


    // сцена с лого патнеров
    let logoparallaxScene = new ScrollMagic.Scene({
            triggerElement: "#about",
            duration: '100%'
        })
        .setTween(logos)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);


    // окно с формой по скроллу    
    // feedbackWindow.fromTo('#feedback', 1, {
    //     y: "0"
    // }, {
    //     y: '-100%',
    //     ease: Cubic.easeOut
    // })


    // сцены с появлениям подсказки о следующем блоке
    $(".section-next").each(function () {

        let sectiontitle = $(this).find('.section-title')
        let sectiontext = $(this).find('.section-text')
        let nexttitle = $(this).find('.next-title')[0]

        let slideIntro = new TimelineMax();
        let nextTitleIntro = new TimelineMax();
        let nextTitleOut = new TimelineMax();


        slideIntro.add([
            TweenMax.from(sectiontitle, 1.5, {
                yPercent: 20,
                ease: Cubic.easeOut
            }),
            TweenMax.from(sectiontext, 1, {
                yPercent: 130,
                ease: Cubic.easeOut
            })
        ]);

        nextTitleIntro
            .add('time')
            .from(nexttitle, 3, {
                opacity: 0,
                yPercent: 180,
                ease: Cubic.easeOut
            }).to(sectiontext, 2, {
                delay: 2,
                opacity: 0,
                ease: Cubic.easeOut
            }, 'time').to(sectiontitle, 1, {
                delay: 2,
                opacity: 0
            }, 'time');

        nextTitleOut
            .to(nexttitle, 1, {
                delay: 4,
                opacity: 0,
                yPercent: -120,
                ease: Cubic.easeOut
            });

        // сцена появления
        new ScrollMagic.Scene({
                triggerElement: this,
                duration: '100%',
                triggerHook: 'onCenter'
            })
            .setTween(slideIntro)
            // .addIndicators() // add indicators (requires plugin)
            .addTo(controller);

        // сцена ухода
        new ScrollMagic.Scene({
                triggerElement: this,
                duration: '100%',
                triggerHook: 'onLeave'
            })
            .setTween(nextTitleIntro)
            // .addIndicators() // add indicators (requires plugin)
            .addTo(controller);

        // сцена пина указателя
        let pinNextTitle = new ScrollMagic.Scene({
                triggerElement: nexttitle,
                duration: '40%',
                triggerHook: 'onCenter'
            })
            .setPin(nexttitle)
            // .addIndicators() // add indicators (requires plugin)
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: '100%',
                triggerHook: 'onLeave'
            })
            .setTween(nextTitleOut)
            // .addIndicators() // add indicators (requires plugin)
            .addTo(controller);

    });

    // красный фон
    let changeBg = new ScrollMagic.Scene({
            triggerElement: "#about",
            duration: '150%',
            triggerHook: 'onCenter'
        })
        .setClassToggle('body', 'red-bg')
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    new ScrollMagic.Scene({
            triggerElement: ".trigger",
            // duration: '100%',
            triggerHook: 'onEnter'
        })
        .on('enter', function () {
            $('#feedback').addClass('modal-section')
        })
        .on('leave', function () {
            $('#feedback').removeClass('modal-section')
        })
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);


    // let pinAbout = new ScrollMagic.Scene({
    //         triggerElement: "#about",
    //         duration: '100%',
    //         triggerHook: 'onLeave'
    //     })
    //     // .setPin('#about')
    //     // .setTween(feedbackWindow)
    //     // .addIndicators() // add indicators (requires plugin)
    //     .addTo(controller);
}

export default pageAnimation;