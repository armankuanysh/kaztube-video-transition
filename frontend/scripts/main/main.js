import { TimelineMax } from "gsap";

function videoPlay() {
  const video = document.querySelector(".video-frame");
  const content = document.querySelector(".js-content");
  const sub = document.querySelector(".video__sub");
  const TL = new TimelineMax();
  let isOpen = false;

  let conf = {
    iframeMouseOver: false
  };
  window.addEventListener("blur", function() {
    if (conf.iframeMouseOver) {
      TL.fromTo(
        content,
        0.5,
        {
          y: 0,
          opacity: 1,
          display: "block"
        },
        {
          y: 100,
          opacity: 0,
          display: "none"
        }
      ).fromTo(
        sub,
        0.5,
        {
          y: -100,
          opacity: 0,
          display: "none"
        },
        {
          y: 0,
          opacity: 1,
          display: "block"
        },
        "-=0.3"
      );
    }
  });

  video.addEventListener("mouseover", function() {
    conf.iframeMouseOver = true;
  });
  video.addEventListener("mouseout", function() {
    conf.iframeMouseOver = false;
  });
}

videoPlay();
