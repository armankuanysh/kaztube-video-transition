<p><br />
Когда приходит новая задача для фронтендера, можно выделить несколько сценариев, как к ней приступать.</p>

<p><strong>1. Это новый проект.</strong></p>

<p>В таком случае нужно уточнить у менеджера, начали ли уже делать бэкенд-часть. Если да, то порядок действий такой:&nbsp;</p>

<p>- уточняем название репозитория в гитлабе<br />
- находим проект<br />
- клонируем его себе на компьютер (git clone ....)<br />
- запускаем команду yarn либо npm install</p>

<p><img alt="" border="0" hspace="0" src="https://pics.rocketfirm.com/margarita/Monosnap_file_uii1v.png" style="border:0px solid black; height:399px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; width:736px" vspace="0" /></p>

<p><br />
<strong>2. Это новый проект</strong>, к которому еще не приступал бэкендер. В этом случае желательно выяснить, будет ли проект использовать bridge. Если да, то порядок действий такой:&nbsp;</p>

<p>- создаем в гитлабе в папке rocketfirm репозиторий с названием проекта<br />
- далее либо запускаем команду $ composer create-project yii2-bridge/app MY_APP_NAME<br />
- либо переходим по ссылке https://github.com/yii2-bridge/app и клонируем проект<br />
- после этого следуем инструкции гитлаба по привязке проекта к новому репозиторию<br />
- запускаем команду yarn либо npm install</p>

<p><img alt="" border="0" hspace="0" src="https://pics.rocketfirm.com/margarita/Monosnap_file_1umsn.png" style="border:0px solid black; height:900px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; width:1623px" vspace="0" /></p>

<p><br />
<strong>3. Это новый проект</strong>, в котором нужна только фронт-часть. В это случае:&nbsp;</p>

<p>- создаем в гитлабе в папке rocketfirm репозиторий с названием проекта<br />
- переходим по ссылке https://gitlab.com/rocketfirm/rocket-frontend-template<br />
- клонируем проект<br />
- после этого следуем инструкции гитлаба по привязке проекта к новому репозиторию<br />
- запускаем команду yarn либо npm install</p>

<p><strong>4. Это старый&nbsp;проект</strong>. В этом случае следуем первому пункту.</p>

<p><br />
<strong>5. Это очень старый проект.</strong>&nbsp;Редко встречается, но в&nbsp;этом случае могут возникуть некоторые сложности:&nbsp;</p>

<p>- в проекте используется bower. Нужно установить bower на компьютер в случае его отсутствия, и из папки проекта запустить команду bower install<br />
- нужно переключиться на более старую версию node. Для этого существуют такие инструменты как <strong>n</strong> либо <strong>nvm</strong>. Переключаемся на&nbsp;более страую версию node, удаляем node_modules и заново их ставим&nbsp;. В этом случае главное не забыть поменять версию node обратно при работе с другими проектами.</p>

<p><br />
<strong>Теперь о самом шаблоне.&nbsp;</strong></p>

<p>Для сборки используется gulp, в качестве шаблонизатора - Nunjucks.<br />
Для запуска проекта используется команда <strong>gulp serve</strong>, для сборки - <strong>gulp build</strong>. Для сборки без html-файлов - <strong>gulp build:prod.</strong></p>

<p>Из плюшек -&nbsp;<br />
1. все файлы&nbsp;в корне папки <strong>views</strong>&nbsp;при сборке автоматически попадают в список страниц в index.html<br />
2. можно настроить загрузку на маркап по команде. Для этого в консоли нужно запустить команду <strong>gulp add-sync</strong>, следовать инструкциям. После этого станет доступна комнада <strong>yarn sync</strong> либо <strong>npm run sync</strong></p>

<p>О логотипе Ракетной фирмы в футере -&nbsp;</p>

<p>В шаблоне есть модуль с html-разметкой, в нем есть декстопный логити, и мобильный, а также возможность отключить мобильный. https://pics.rocketfirm.com/margarita/Monosnap_file_c7gtb.png<br />
В папке styles есть модуль со стилями для логотипа, там удобно реализована возможность менять цвета согласно дизайну https://pics.rocketfirm.com/margarita/Monosnap_file_0vghy.png</p>
